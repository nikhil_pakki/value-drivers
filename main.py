# import packages
import os
import re
import pandas as pd
import numpy as np
from flask import Flask, Request, Response, render_template, request
from flask import send_from_directory
from flask_restful import Resource, Api
import requests

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'logs'


def main():
    # app.config['SERVER_NAME'] = 'flashservice'
    app.run(host="0.0.0.0", port=80, threaded=True, debug=True)


@app.route('/')
def upload_file():
    return render_template('upload_new_button.html')


@app.route('/uploader', methods=['GET', 'POST'])
def upload_status():
    if request.method == 'POST':
        # start_date = request.form['start_date']
        # end_date = request.form['end_date']
        # print(f'Fetching the required data from {start_date} to {end_date}')
        f = request.files['file']
        ip = request.access_route[0]
        # print(ip)
        from flask import send_file
        import Value_Drivers as vd

        if not os.path.exists(ip):
            os.mkdir(ip)
        input_file_path = os.path.join(ip, f.filename)
        f.save(input_file_path)
        driver_input = vd.InputDriver(input_file_path)
        driver_input.generate_report()
        return send_file('Value_Drivers.xlsx', as_attachment=True)

        # f.save(f.filename)
        # return f'<h1>Status</h1>' \
        #        f'{f.filename} has been uploaded successfully'


if __name__ == '__main__':
    main()
